package andrii.masalykin.youtubesearchapp.ui

import andrii.masalykin.youtubesearchapp.R
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class SearchActivity : AppCompatActivity() {

    private lateinit var searchPresenter: SearchPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        searchPresenter = SearchPresenter()

        val editText = findViewById<EditText>(R.id.search_edit_text)
        editText.doAfterTextChanged { doAnotherSearch(it.toString()) }

        val resultsView = findViewById<RecyclerView>(R.id.search_results)
        resultsView.adapter = ResultsRecyclerAdapter()
        resultsView.layoutManager = LinearLayoutManager(this)
    }

    private fun doAnotherSearch(searchPhrase: String) {
        searchPresenter.doAnotherSearch(searchPhrase)
    }

}