package andrii.masalykin.youtubesearchapp.ui

import andrii.masalykin.youtubesearchapp.data.SearchProvider
import andrii.masalykin.youtubesearchapp.data.YoutubeSearchResult
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchPresenter {
    private val TAG = "SearchPresenter"
    private val searchProvider: SearchProvider

    init {
        searchProvider = SearchProvider()
    }

    fun doAnotherSearch(searchPhrase: String) {
        searchProvider.searchService.search(searchPhrase)
            .enqueue(object : Callback<YoutubeSearchResult> {
                override fun onFailure(call: Call<YoutubeSearchResult>, t: Throwable) {
                    Log.e(TAG, "onFailure: $t")
                }

                override fun onResponse(
                    call: Call<YoutubeSearchResult>,
                    response: Response<YoutubeSearchResult>
                ) {
                    Log.d(TAG, "onResponse: ${response.body()}")
                }
            })
    }

}