package andrii.masalykin.youtubesearchapp.ui

import andrii.masalykin.youtubesearchapp.data.YoutubeSearchResult
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ResultsRecyclerAdapter : RecyclerView.Adapter<ResultsRecyclerAdapter.ResultViewHolder>() {

    private val results: MutableList<YoutubeSearchResult> = ArrayList()

    fun setResults(results: List<YoutubeSearchResult>) {
        this.results.clear()
        this.results.addAll(results)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultViewHolder {
        TODO("Not yet implemented")
    }

    override fun onBindViewHolder(holder: ResultViewHolder, position: Int) {
        TODO("Not yet implemented")
    }

    override fun getItemCount() = this.results.size

    class ResultViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}