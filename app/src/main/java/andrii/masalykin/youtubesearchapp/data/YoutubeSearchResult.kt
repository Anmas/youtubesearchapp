package andrii.masalykin.youtubesearchapp.data

data class YoutubeSearchResult(
    val token: String?,
    val videos: List<VideoItem>?
)

data class VideoItem(
    val previewUrl: String,
    val videoUrl: String
)