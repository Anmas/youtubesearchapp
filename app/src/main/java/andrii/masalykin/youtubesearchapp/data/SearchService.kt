package andrii.masalykin.youtubesearchapp.data

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchService {
    @GET("results")
    fun search(@Query("search_query") searchPhrase: String): Call<YoutubeSearchResult>

    @GET("v1/search?key={key}&prettyPrint=false")
    fun searchNextPage(key: String): Call<String>
}