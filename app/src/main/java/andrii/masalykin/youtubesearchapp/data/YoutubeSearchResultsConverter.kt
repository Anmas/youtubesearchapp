package andrii.masalykin.youtubesearchapp.data

import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

class YoutubeSearchResultsConverterFactory : Converter.Factory() {
    override fun responseBodyConverter(
        type: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? {
        return YoutubeSearchResultsConverter()
    }

}

class YoutubeSearchResultsConverter : Converter<ResponseBody, YoutubeSearchResult> {
    override fun convert(value: ResponseBody): YoutubeSearchResult? {
        val string = value.string()
        val apiMatch = "(?<=INNERTUBE_API_KEY\":\").*?(?=\")".toRegex()
        val apiKey = apiMatch.find(string)?.value

        val thumbnailMatch = "".toRegex()
        val videoUrlMatch = "".toRegex()

        val videos = ArrayList<VideoItem>()
        return YoutubeSearchResult(apiKey, videos)
    }
}