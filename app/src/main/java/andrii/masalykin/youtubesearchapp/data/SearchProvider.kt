package andrii.masalykin.youtubesearchapp.data

import retrofit2.Retrofit

class SearchProvider {
    internal val searchService: SearchService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://www.youtube.com/")
            .addConverterFactory(YoutubeSearchResultsConverterFactory())
            .build()
        searchService = retrofit.create(SearchService::class.java)
    }


}